
# Regression
### data-set source: 
https://datahack.analyticsvidhya.com/contest/practice-problem-big-mart-sales-iii/

### data-set description: 
The data scientists at BigMart have collected 2013 sales data for 1559 products across 10 stores in different cities. Also, certain attributes of each product and store have been defined. The aim is to build a predictive model and find out the sales of each product at a particular store.

Using this model, BigMart will try to understand the properties of products and stores which play a key role in increasing sales.

 

Please note that the data may have missing values as some stores might not report all the data due to technical glitches. Hence, it will be required to treat them accordingly.

### Schema details:

Variable: Description

Item_Identifier: Unique product ID

Item_Weight: Weight of product

Item_Fat_Content: Whether the product is low fat or not

Item_Visibility: The % of total display area of all products in a store allocated to the particular product

Item_Type: The category to which the product belongs

Item_MRP: Maximum Retail Price (list price) of the product

Outlet_Identifier: Unique store ID

Outlet_Establishment_Year: The year in which store was established

Outlet_Size: The size of the store in terms of ground area covered

Outlet_Location_Type: The type of city in which the store is located

Outlet_Type: Whether the outlet is just a grocery store or some sort of supermarket

Item_Outlet_Sales: Sales of the product in the particulat store. This is the outcome variable to be predicted.



```python
# reading the data-set and setting header=True
df = spark.read.csv("/user/centos/Train_UWu5bXk.csv",header=True)
```


```python
df.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: string (nullable = true)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: string (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: string (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: string (nullable = true)
     |-- Outlet_Size: string (nullable = true)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: string (nullable = true)
    



```python
from pyspark.sql.types import StructField,StructType,IntegerType,DoubleType,StringType
```


```python
df.show(n=2)
```

    +---------------+-----------+----------------+---------------+-----------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |Item_Identifier|Item_Weight|Item_Fat_Content|Item_Visibility|  Item_Type|Item_MRP|Outlet_Identifier|Outlet_Establishment_Year|Outlet_Size|Outlet_Location_Type|      Outlet_Type|Item_Outlet_Sales|
    +---------------+-----------+----------------+---------------+-----------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |          FDA15|        9.3|         Low Fat|    0.016047301|      Dairy|249.8092|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         3735.138|
    |          DRC01|       5.92|         Regular|    0.019278216|Soft Drinks| 48.2692|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         443.4228|
    +---------------+-----------+----------------+---------------+-----------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    only showing top 2 rows
    



```python
# defining schema:
fields = [StructField("Item_Identifier",StringType(),True),
          StructField("Item_Weight",DoubleType(),True),
          StructField("Item_Fat_Content",StringType(),True),
          StructField("Item_Visibility",DoubleType(),True),
          StructField("Item_Type",StringType(),True),
          StructField("Item_MRP",DoubleType(),True),
          StructField("Outlet_Identifier",StringType(),True),
          StructField("Outlet_Establishment_Year",IntegerType(),True),
          StructField("Outlet_Size",StringType(),True),
          StructField("Outlet_Location_Type",StringType(),True),
          StructField("Outlet_Type",StringType(),True),
          StructField("Item_Outlet_Sales",DoubleType(),True)
         ]
```


```python
df_schema = StructType(fields)
```


```python
# loading the data-set with the right schema:
df2 = spark.read.csv("/user/centos/Train_UWu5bXk.csv",header=True,schema = df_schema)
```


```python
df2.show()
```

    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |Item_Identifier|Item_Weight|Item_Fat_Content|Item_Visibility|           Item_Type|Item_MRP|Outlet_Identifier|Outlet_Establishment_Year|Outlet_Size|Outlet_Location_Type|      Outlet_Type|Item_Outlet_Sales|
    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |          FDA15|        9.3|         Low Fat|    0.016047301|               Dairy|249.8092|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         3735.138|
    |          DRC01|       5.92|         Regular|    0.019278216|         Soft Drinks| 48.2692|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         443.4228|
    |          FDN15|       17.5|         Low Fat|    0.016760075|                Meat| 141.618|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|          2097.27|
    |          FDX07|       19.2|         Regular|            0.0|Fruits and Vegeta...| 182.095|           OUT010|                     1998|       null|              Tier 3|    Grocery Store|           732.38|
    |          NCD19|       8.93|         Low Fat|            0.0|           Household| 53.8614|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         994.7052|
    |          FDP36|     10.395|         Regular|            0.0|        Baking Goods| 51.4008|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         556.6088|
    |          FDO10|      13.65|         Regular|    0.012741089|         Snack Foods| 57.6588|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         343.5528|
    |          FDP10|       null|         Low Fat|    0.127469857|         Snack Foods|107.7622|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|        4022.7636|
    |          FDH17|       16.2|         Regular|    0.016687114|        Frozen Foods| 96.9726|           OUT045|                     2002|       null|              Tier 2|Supermarket Type1|        1076.5986|
    |          FDU28|       19.2|         Regular|     0.09444959|        Frozen Foods|187.8214|           OUT017|                     2007|       null|              Tier 2|Supermarket Type1|         4710.535|
    |          FDY07|       11.8|         Low Fat|            0.0|Fruits and Vegeta...| 45.5402|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1516.0266|
    |          FDA03|       18.5|         Regular|    0.045463773|               Dairy|144.1102|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|         2187.153|
    |          FDX32|       15.1|         Regular|      0.1000135|Fruits and Vegeta...|145.4786|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1589.2646|
    |          FDS46|       17.6|         Regular|    0.047257328|         Snack Foods|119.6782|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        2145.2076|
    |          FDF32|      16.35|         Low Fat|      0.0680243|Fruits and Vegeta...|196.4426|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         1977.426|
    |          FDP49|        9.0|         Regular|    0.069088961|           Breakfast| 56.3614|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        1547.3192|
    |          NCB42|       11.8|         Low Fat|    0.008596051|  Health and Hygiene|115.3492|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|        1621.8888|
    |          FDP49|        9.0|         Regular|    0.069196376|           Breakfast| 54.3614|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         718.3982|
    |          DRI11|       null|         Low Fat|    0.034237682|         Hard Drinks|113.2834|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|         2303.668|
    |          FDU02|      13.35|         Low Fat|     0.10249212|               Dairy|230.5352|           OUT035|                     2004|      Small|              Tier 2|Supermarket Type1|        2748.4224|
    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    only showing top 20 rows
    



```python
# finding number of rows of the dataframe:
num_rows = df2.count()
```


```python
import pyspark.sql.functions as funct
```


```python
#finding missing values by generating a dictionary:
missing_val_dict=df2.agg(*[(num_rows - funct.count(each_col)).alias(each_col+"_missing") for each_col in df2.columns]).rdd.collect()[0].asDict()

# .rdd.collect gives an array of Row Objects. In this case, it will have just one element. That element is indexed.
# and then the retrieved Row object is converted to dictionary.
```


```python
#iterating through the dictionary and getting just those columns which have missing values.
for k,v in missing_val_dict.items():
    if (v>0): # checking if a value of dictionary element has value greater than zero i.e. it has missing values
        print (k+":"+str(v)) # just printing the result.
```

    Item_Weight_missing:1463
    Outlet_Size_missing:2410



```python
df2.columns
```




    ['Item_Identifier',
     'Item_Weight',
     'Item_Fat_Content',
     'Item_Visibility',
     'Item_Type',
     'Item_MRP',
     'Outlet_Identifier',
     'Outlet_Establishment_Year',
     'Outlet_Size',
     'Outlet_Location_Type',
     'Outlet_Type',
     'Item_Outlet_Sales']




```python
df2.select("Item_Weight","Outlet_Size").show()
```

    +-----------+-----------+
    |Item_Weight|Outlet_Size|
    +-----------+-----------+
    |        9.3|     Medium|
    |       5.92|     Medium|
    |       17.5|     Medium|
    |       19.2|       null|
    |       8.93|       High|
    |     10.395|     Medium|
    |      13.65|       High|
    |       null|     Medium|
    |       16.2|       null|
    |       19.2|       null|
    |       11.8|     Medium|
    |       18.5|      Small|
    |       15.1|     Medium|
    |       17.6|      Small|
    |      16.35|       High|
    |        9.0|      Small|
    |       11.8|     Medium|
    |        9.0|     Medium|
    |       null|     Medium|
    |      13.35|      Small|
    +-----------+-----------+
    only showing top 20 rows
    



```python
# as one of the columns is Numeric and the other is categorical so just creating a dictionary which will have 
# mean of the numeric column and mode of the other column for imputation purposes;
```


```python
#finding mode:
d1=df2.groupBy("Outlet_Size").count().sort("count",ascending=False).first().asDict()
```


```python
d1
```




    {'Outlet_Size': u'Medium', 'count': 2793}




```python
impute_dict=df2.agg(funct.mean("Item_Weight").alias("Item_Weight")).rdd.collect()[0].asDict()
```


```python
impute_dict["Outlet_Size"]="Medium"
```


```python
impute_dict
```




    {'Item_Weight': 12.857645184136183, 'Outlet_Size': 'Medium'}




```python
df2.show()
```

    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |Item_Identifier|Item_Weight|Item_Fat_Content|Item_Visibility|           Item_Type|Item_MRP|Outlet_Identifier|Outlet_Establishment_Year|Outlet_Size|Outlet_Location_Type|      Outlet_Type|Item_Outlet_Sales|
    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |          FDA15|        9.3|         Low Fat|    0.016047301|               Dairy|249.8092|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         3735.138|
    |          DRC01|       5.92|         Regular|    0.019278216|         Soft Drinks| 48.2692|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         443.4228|
    |          FDN15|       17.5|         Low Fat|    0.016760075|                Meat| 141.618|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|          2097.27|
    |          FDX07|       19.2|         Regular|            0.0|Fruits and Vegeta...| 182.095|           OUT010|                     1998|       null|              Tier 3|    Grocery Store|           732.38|
    |          NCD19|       8.93|         Low Fat|            0.0|           Household| 53.8614|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         994.7052|
    |          FDP36|     10.395|         Regular|            0.0|        Baking Goods| 51.4008|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         556.6088|
    |          FDO10|      13.65|         Regular|    0.012741089|         Snack Foods| 57.6588|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         343.5528|
    |          FDP10|       null|         Low Fat|    0.127469857|         Snack Foods|107.7622|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|        4022.7636|
    |          FDH17|       16.2|         Regular|    0.016687114|        Frozen Foods| 96.9726|           OUT045|                     2002|       null|              Tier 2|Supermarket Type1|        1076.5986|
    |          FDU28|       19.2|         Regular|     0.09444959|        Frozen Foods|187.8214|           OUT017|                     2007|       null|              Tier 2|Supermarket Type1|         4710.535|
    |          FDY07|       11.8|         Low Fat|            0.0|Fruits and Vegeta...| 45.5402|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1516.0266|
    |          FDA03|       18.5|         Regular|    0.045463773|               Dairy|144.1102|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|         2187.153|
    |          FDX32|       15.1|         Regular|      0.1000135|Fruits and Vegeta...|145.4786|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1589.2646|
    |          FDS46|       17.6|         Regular|    0.047257328|         Snack Foods|119.6782|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        2145.2076|
    |          FDF32|      16.35|         Low Fat|      0.0680243|Fruits and Vegeta...|196.4426|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         1977.426|
    |          FDP49|        9.0|         Regular|    0.069088961|           Breakfast| 56.3614|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        1547.3192|
    |          NCB42|       11.8|         Low Fat|    0.008596051|  Health and Hygiene|115.3492|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|        1621.8888|
    |          FDP49|        9.0|         Regular|    0.069196376|           Breakfast| 54.3614|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         718.3982|
    |          DRI11|       null|         Low Fat|    0.034237682|         Hard Drinks|113.2834|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|         2303.668|
    |          FDU02|      13.35|         Low Fat|     0.10249212|               Dairy|230.5352|           OUT035|                     2004|      Small|              Tier 2|Supermarket Type1|        2748.4224|
    +---------------+-----------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    only showing top 20 rows
    



```python
df3 = df2.na.fill(impute_dict)
```


```python
df3.show()
```

    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |Item_Identifier|       Item_Weight|Item_Fat_Content|Item_Visibility|           Item_Type|Item_MRP|Outlet_Identifier|Outlet_Establishment_Year|Outlet_Size|Outlet_Location_Type|      Outlet_Type|Item_Outlet_Sales|
    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    |          FDA15|               9.3|         Low Fat|    0.016047301|               Dairy|249.8092|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         3735.138|
    |          DRC01|              5.92|         Regular|    0.019278216|         Soft Drinks| 48.2692|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         443.4228|
    |          FDN15|              17.5|         Low Fat|    0.016760075|                Meat| 141.618|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|          2097.27|
    |          FDX07|              19.2|         Regular|            0.0|Fruits and Vegeta...| 182.095|           OUT010|                     1998|     Medium|              Tier 3|    Grocery Store|           732.38|
    |          NCD19|              8.93|         Low Fat|            0.0|           Household| 53.8614|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         994.7052|
    |          FDP36|            10.395|         Regular|            0.0|        Baking Goods| 51.4008|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         556.6088|
    |          FDO10|             13.65|         Regular|    0.012741089|         Snack Foods| 57.6588|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         343.5528|
    |          FDP10|12.857645184136183|         Low Fat|    0.127469857|         Snack Foods|107.7622|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|        4022.7636|
    |          FDH17|              16.2|         Regular|    0.016687114|        Frozen Foods| 96.9726|           OUT045|                     2002|     Medium|              Tier 2|Supermarket Type1|        1076.5986|
    |          FDU28|              19.2|         Regular|     0.09444959|        Frozen Foods|187.8214|           OUT017|                     2007|     Medium|              Tier 2|Supermarket Type1|         4710.535|
    |          FDY07|              11.8|         Low Fat|            0.0|Fruits and Vegeta...| 45.5402|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1516.0266|
    |          FDA03|              18.5|         Regular|    0.045463773|               Dairy|144.1102|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|         2187.153|
    |          FDX32|              15.1|         Regular|      0.1000135|Fruits and Vegeta...|145.4786|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1589.2646|
    |          FDS46|              17.6|         Regular|    0.047257328|         Snack Foods|119.6782|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        2145.2076|
    |          FDF32|             16.35|         Low Fat|      0.0680243|Fruits and Vegeta...|196.4426|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         1977.426|
    |          FDP49|               9.0|         Regular|    0.069088961|           Breakfast| 56.3614|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        1547.3192|
    |          NCB42|              11.8|         Low Fat|    0.008596051|  Health and Hygiene|115.3492|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|        1621.8888|
    |          FDP49|               9.0|         Regular|    0.069196376|           Breakfast| 54.3614|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         718.3982|
    |          DRI11|12.857645184136183|         Low Fat|    0.034237682|         Hard Drinks|113.2834|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|         2303.668|
    |          FDU02|             13.35|         Low Fat|     0.10249212|               Dairy|230.5352|           OUT035|                     2004|      Small|              Tier 2|Supermarket Type1|        2748.4224|
    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+
    only showing top 20 rows
    



```python
#finding missing values by generating a dictionary:
df3.agg(*[(num_rows - funct.count(each_col)).alias(each_col+"_missing") for each_col in df2.columns]).rdd.collect()[0].asDict()

# .rdd.collect gives an array of Row Objects. In this case, it will have just one element. That element is indexed.
# and then the retrieved Row object is converted to dictionary.
```




    {'Item_Fat_Content_missing': 0,
     'Item_Identifier_missing': 0,
     'Item_MRP_missing': 0,
     'Item_Outlet_Sales_missing': 0,
     'Item_Type_missing': 0,
     'Item_Visibility_missing': 0,
     'Item_Weight_missing': 0,
     'Outlet_Establishment_Year_missing': 0,
     'Outlet_Identifier_missing': 0,
     'Outlet_Location_Type_missing': 0,
     'Outlet_Size_missing': 0,
     'Outlet_Type_missing': 0}




```python
# now with the missing values removed, 
# lets build regression model with just numeric columns:

# lets just build the model with establishment year and item retail price;
df3.columns
```




    ['Item_Identifier',
     'Item_Weight',
     'Item_Fat_Content',
     'Item_Visibility',
     'Item_Type',
     'Item_MRP',
     'Outlet_Identifier',
     'Outlet_Establishment_Year',
     'Outlet_Size',
     'Outlet_Location_Type',
     'Outlet_Type',
     'Item_Outlet_Sales']




```python
# so specifically using: 
#Outlet_Establishment_Year
#Item_MRP
df3.select("Outlet_Establishment_Year","Item_MRP").printSchema()
```

    root
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Item_MRP: double (nullable = true)
    



```python
#both are numeric columns. lets just build linear regression with no standardization/scaling.

from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
```


```python
v_assembler = VectorAssembler(inputCols=["Outlet_Establishment_Year","Item_MRP"],
                            outputCol="features")
```


```python
df4 = v_assembler.transform(df3)
```


```python
df4.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
     |-- features: vector (nullable = true)
    



```python
df5=df4.withColumn("label",df4.Item_Outlet_Sales).select("features","label")
```


```python

from pyspark.ml.regression import LinearRegression
lr = LinearRegression(maxIter=10, regParam=0, elasticNetParam=0)
#pyspark's LR uses ElasticNet regularization, which is a weighted sum of L1 and L2 terms; weight is elasticNetParam. 
# So with elasticNetParam=0 you get L2 regularization, and regParam is L2 regularization coefficient; 
#with elasticNetParam=1 you get L1 regularization, and regParam is L1 regularization coefficient.
```


```python
train_df,test_df = df5.randomSplit([0.7,0.3],seed=100)
```


```python
lrModel = lr.fit(train_df)
```


```python
training_summary = lrModel.summary
```


```python
training_summary.r2 # Rsquared value is .32 i.e. 32% of the variance is explained by this model.
```




    0.3238565090752391




```python
lrModel.coefficients # their respective parameter values are: -10.9, 15
# so the first param corresponds to establishment year and the second corresponds to item retail price;
```




    DenseVector([-10.9664, 15.4148])




```python
lrModel.numFeatures # so 2 features. 
```




    2




```python
#to do: 
    # add another feature i.e. weight and provide the R2 value in this cell:

```


```python
# lets include all the features:
df3.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
    



```python
df4=df3
```


```python
df4.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
    



```python
# As a part of pre-processing, doing StringIndexing and then OneHotEncoding on the string columns:
from pyspark.ml.feature import StringIndexer
string_cols = ["Item_Fat_Content","Item_Type","Outlet_Identifier","Outlet_Size","Outlet_Location_Type","Outlet_Type"]
for col in string_cols:
    string_indexer=StringIndexer(inputCol=col,outputCol=col+"_index")
    model=string_indexer.fit(df4)
    df4=model.transform(df4)
```


```python
df4.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
     |-- Item_Fat_Content_index: double (nullable = false)
     |-- Item_Type_index: double (nullable = false)
     |-- Outlet_Identifier_index: double (nullable = false)
     |-- Outlet_Size_index: double (nullable = false)
     |-- Outlet_Location_Type_index: double (nullable = false)
     |-- Outlet_Type_index: double (nullable = false)
    



```python
# use one hot encoder on the indexed string columns:

from pyspark.ml.feature import OneHotEncoderEstimator
one_hot_encoder = OneHotEncoderEstimator(inputCols=[x for x in df4.columns if "_index" in x],
                                        outputCols=[x+"_vec" for x in df4.columns if "_index" in x])

```


```python
one_hot_model=one_hot_encoder.fit(df4)
```


```python
df5=one_hot_model.transform(df4)
```


```python
df5.show()
```

    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+----------------------+---------------+-----------------------+-----------------+--------------------------+-----------------+---------------------+-------------------+---------------------------+--------------------------+---------------------+------------------------------+
    |Item_Identifier|       Item_Weight|Item_Fat_Content|Item_Visibility|           Item_Type|Item_MRP|Outlet_Identifier|Outlet_Establishment_Year|Outlet_Size|Outlet_Location_Type|      Outlet_Type|Item_Outlet_Sales|Item_Fat_Content_index|Item_Type_index|Outlet_Identifier_index|Outlet_Size_index|Outlet_Location_Type_index|Outlet_Type_index|Outlet_Type_index_vec|Item_Type_index_vec|Outlet_Identifier_index_vec|Item_Fat_Content_index_vec|Outlet_Size_index_vec|Outlet_Location_Type_index_vec|
    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+----------------------+---------------+-----------------------+-----------------+--------------------------+-----------------+---------------------+-------------------+---------------------------+--------------------------+---------------------+------------------------------+
    |          FDA15|               9.3|         Low Fat|    0.016047301|               Dairy|249.8092|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         3735.138|                   0.0|            4.0|                    2.0|              0.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[4],[1.0])|              (9,[2],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                     (2,[],[])|
    |          DRC01|              5.92|         Regular|    0.019278216|         Soft Drinks| 48.2692|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         443.4228|                   1.0|            8.0|                    6.0|              0.0|                       0.0|              3.0|            (3,[],[])|     (15,[8],[1.0])|              (9,[6],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          FDN15|              17.5|         Low Fat|    0.016760075|                Meat| 141.618|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|          2097.27|                   0.0|            9.0|                    2.0|              0.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[9],[1.0])|              (9,[2],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                     (2,[],[])|
    |          FDX07|              19.2|         Regular|            0.0|Fruits and Vegeta...| 182.095|           OUT010|                     1998|     Medium|              Tier 3|    Grocery Store|           732.38|                   1.0|            0.0|                    8.0|              0.0|                       0.0|              1.0|        (3,[1],[1.0])|     (15,[0],[1.0])|              (9,[8],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          NCD19|              8.93|         Low Fat|            0.0|           Household| 53.8614|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         994.7052|                   0.0|            2.0|                    1.0|              2.0|                       0.0|              0.0|        (3,[0],[1.0])|     (15,[2],[1.0])|              (9,[1],[1.0])|             (4,[0],[1.0])|            (2,[],[])|                 (2,[0],[1.0])|
    |          FDP36|            10.395|         Regular|            0.0|        Baking Goods| 51.4008|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|         556.6088|                   1.0|            6.0|                    6.0|              0.0|                       0.0|              3.0|            (3,[],[])|     (15,[6],[1.0])|              (9,[6],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          FDO10|             13.65|         Regular|    0.012741089|         Snack Foods| 57.6588|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         343.5528|                   1.0|            1.0|                    1.0|              2.0|                       0.0|              0.0|        (3,[0],[1.0])|     (15,[1],[1.0])|              (9,[1],[1.0])|             (4,[1],[1.0])|            (2,[],[])|                 (2,[0],[1.0])|
    |          FDP10|12.857645184136183|         Low Fat|    0.127469857|         Snack Foods|107.7622|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|        4022.7636|                   0.0|            1.0|                    0.0|              0.0|                       0.0|              2.0|        (3,[2],[1.0])|     (15,[1],[1.0])|              (9,[0],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          FDH17|              16.2|         Regular|    0.016687114|        Frozen Foods| 96.9726|           OUT045|                     2002|     Medium|              Tier 2|Supermarket Type1|        1076.5986|                   1.0|            3.0|                    5.0|              0.0|                       1.0|              0.0|        (3,[0],[1.0])|     (15,[3],[1.0])|              (9,[5],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                 (2,[1],[1.0])|
    |          FDU28|              19.2|         Regular|     0.09444959|        Frozen Foods|187.8214|           OUT017|                     2007|     Medium|              Tier 2|Supermarket Type1|         4710.535|                   1.0|            3.0|                    7.0|              0.0|                       1.0|              0.0|        (3,[0],[1.0])|     (15,[3],[1.0])|              (9,[7],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                 (2,[1],[1.0])|
    |          FDY07|              11.8|         Low Fat|            0.0|Fruits and Vegeta...| 45.5402|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1516.0266|                   0.0|            0.0|                    2.0|              0.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[0],[1.0])|              (9,[2],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                     (2,[],[])|
    |          FDA03|              18.5|         Regular|    0.045463773|               Dairy|144.1102|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|         2187.153|                   1.0|            4.0|                    3.0|              1.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[4],[1.0])|              (9,[3],[1.0])|             (4,[1],[1.0])|        (2,[1],[1.0])|                     (2,[],[])|
    |          FDX32|              15.1|         Regular|      0.1000135|Fruits and Vegeta...|145.4786|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|        1589.2646|                   1.0|            0.0|                    2.0|              0.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[0],[1.0])|              (9,[2],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                     (2,[],[])|
    |          FDS46|              17.6|         Regular|    0.047257328|         Snack Foods|119.6782|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        2145.2076|                   1.0|            1.0|                    3.0|              1.0|                       2.0|              0.0|        (3,[0],[1.0])|     (15,[1],[1.0])|              (9,[3],[1.0])|             (4,[1],[1.0])|        (2,[1],[1.0])|                     (2,[],[])|
    |          FDF32|             16.35|         Low Fat|      0.0680243|Fruits and Vegeta...|196.4426|           OUT013|                     1987|       High|              Tier 3|Supermarket Type1|         1977.426|                   0.0|            0.0|                    1.0|              2.0|                       0.0|              0.0|        (3,[0],[1.0])|     (15,[0],[1.0])|              (9,[1],[1.0])|             (4,[0],[1.0])|            (2,[],[])|                 (2,[0],[1.0])|
    |          FDP49|               9.0|         Regular|    0.069088961|           Breakfast| 56.3614|           OUT046|                     1997|      Small|              Tier 1|Supermarket Type1|        1547.3192|                   1.0|           14.0|                    3.0|              1.0|                       2.0|              0.0|        (3,[0],[1.0])|    (15,[14],[1.0])|              (9,[3],[1.0])|             (4,[1],[1.0])|        (2,[1],[1.0])|                     (2,[],[])|
    |          NCB42|              11.8|         Low Fat|    0.008596051|  Health and Hygiene|115.3492|           OUT018|                     2009|     Medium|              Tier 3|Supermarket Type2|        1621.8888|                   0.0|            7.0|                    6.0|              0.0|                       0.0|              3.0|            (3,[],[])|     (15,[7],[1.0])|              (9,[6],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          FDP49|               9.0|         Regular|    0.069196376|           Breakfast| 54.3614|           OUT049|                     1999|     Medium|              Tier 1|Supermarket Type1|         718.3982|                   1.0|           14.0|                    2.0|              0.0|                       2.0|              0.0|        (3,[0],[1.0])|    (15,[14],[1.0])|              (9,[2],[1.0])|             (4,[1],[1.0])|        (2,[0],[1.0])|                     (2,[],[])|
    |          DRI11|12.857645184136183|         Low Fat|    0.034237682|         Hard Drinks|113.2834|           OUT027|                     1985|     Medium|              Tier 3|Supermarket Type3|         2303.668|                   0.0|           11.0|                    0.0|              0.0|                       0.0|              2.0|        (3,[2],[1.0])|    (15,[11],[1.0])|              (9,[0],[1.0])|             (4,[0],[1.0])|        (2,[0],[1.0])|                 (2,[0],[1.0])|
    |          FDU02|             13.35|         Low Fat|     0.10249212|               Dairy|230.5352|           OUT035|                     2004|      Small|              Tier 2|Supermarket Type1|        2748.4224|                   0.0|            4.0|                    4.0|              1.0|                       1.0|              0.0|        (3,[0],[1.0])|     (15,[4],[1.0])|              (9,[4],[1.0])|             (4,[0],[1.0])|        (2,[1],[1.0])|                 (2,[1],[1.0])|
    +---------------+------------------+----------------+---------------+--------------------+--------+-----------------+-------------------------+-----------+--------------------+-----------------+-----------------+----------------------+---------------+-----------------------+-----------------+--------------------------+-----------------+---------------------+-------------------+---------------------------+--------------------------+---------------------+------------------------------+
    only showing top 20 rows
    



```python
df5.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
     |-- Item_Fat_Content_index: double (nullable = false)
     |-- Item_Type_index: double (nullable = false)
     |-- Outlet_Identifier_index: double (nullable = false)
     |-- Outlet_Size_index: double (nullable = false)
     |-- Outlet_Location_Type_index: double (nullable = false)
     |-- Outlet_Type_index: double (nullable = false)
     |-- Outlet_Type_index_vec: vector (nullable = true)
     |-- Item_Type_index_vec: vector (nullable = true)
     |-- Outlet_Identifier_index_vec: vector (nullable = true)
     |-- Item_Fat_Content_index_vec: vector (nullable = true)
     |-- Outlet_Size_index_vec: vector (nullable = true)
     |-- Outlet_Location_Type_index_vec: vector (nullable = true)
    



```python
feature_cols=["Item_Weight","Item_Fat_Content_index_vec","Item_Visibility","Item_Type_index_vec",
                                              "Item_MRP","Outlet_Identifier_index_vec","Outlet_Establishment_Year",
                                              "Outlet_Size_index_vec","Outlet_Location_Type_index_vec","Outlet_Type_index_vec"]
```


```python
# now use vector assembler to gather all the columne
vec_assembler_all = VectorAssembler(inputCols=feature_cols,
                                   outputCol="features")
```


```python
df6 = vec_assembler_all.transform(df5)
```


```python
df6.printSchema()
```

    root
     |-- Item_Identifier: string (nullable = true)
     |-- Item_Weight: double (nullable = false)
     |-- Item_Fat_Content: string (nullable = true)
     |-- Item_Visibility: double (nullable = true)
     |-- Item_Type: string (nullable = true)
     |-- Item_MRP: double (nullable = true)
     |-- Outlet_Identifier: string (nullable = true)
     |-- Outlet_Establishment_Year: integer (nullable = true)
     |-- Outlet_Size: string (nullable = false)
     |-- Outlet_Location_Type: string (nullable = true)
     |-- Outlet_Type: string (nullable = true)
     |-- Item_Outlet_Sales: double (nullable = true)
     |-- Item_Fat_Content_index: double (nullable = false)
     |-- Item_Type_index: double (nullable = false)
     |-- Outlet_Identifier_index: double (nullable = false)
     |-- Outlet_Size_index: double (nullable = false)
     |-- Outlet_Location_Type_index: double (nullable = false)
     |-- Outlet_Type_index: double (nullable = false)
     |-- Outlet_Type_index_vec: vector (nullable = true)
     |-- Item_Type_index_vec: vector (nullable = true)
     |-- Outlet_Identifier_index_vec: vector (nullable = true)
     |-- Item_Fat_Content_index_vec: vector (nullable = true)
     |-- Outlet_Size_index_vec: vector (nullable = true)
     |-- Outlet_Location_Type_index_vec: vector (nullable = true)
     |-- features: vector (nullable = true)
    



```python
df7=df6.withColumn("label",df6.Item_Outlet_Sales).select("features","label")
```


```python
lrModel3=lr.fit(df7)
```


```python
training_summary_3=lrModel3.summary
```


```python
training_summary_3.r2 #increased to .56
```




    0.5637488236940518




```python
# apply regularization? set regParam to 0.3 and elasticNet so that its ridge regression and see R2?

```


```python
# apply regularization: set regParam to 0.3 and elasticNet so that you get lasso regression and check R2?
```
