

```python
# generating a sample DataFrame:
df_miss = spark.createDataFrame([
        (1, 143.5, 5.6, 28,   'M',  100000),
        (2, 167.2, 5.4, 45,   'M',  None),
        (3, None , 5.2, None, None, None),
        (4, 144.5, 5.9, 33,   'M',  None),
        (5, 133.2, 5.7, 54,   'F',  None),
        (6, 124.1, 5.2, None, 'F',  None),
        (7, 129.2, 5.3, 42,   'M',  76000),
    ], ['id', 'weight', 'height', 'age', 'gender', 'income'])
```


```python
"hello".upper()
```




    'HELLO'




```python
sc.parallelize(["irfan","elahi","melbourne"]).map(lambda x: x.upper()).collect()
```




    ['IRFAN', 'ELAHI', 'MELBOURNE']




```python
from pyspark.sql import Row

df_num_missing = df_miss.rdd.map(lambda each_row: \
                Row(each_row['id'],sum([c == None for c in each_row])))\
.toDF(["id","numMissing"]).sort("numMissing")

#output shows that row with ID 3 has the maximum number of missing elements.
```


```python
df_num_missing.show()
```

    +---+----------+
    | id|numMissing|
    +---+----------+
    |  7|         0|
    |  1|         0|
    |  4|         1|
    |  5|         1|
    |  2|         1|
    |  6|         2|
    |  3|         4|
    +---+----------+
    



```python
df_miss.where("id == 3").show()
```

    +---+------+------+----+------+------+
    | id|weight|height| age|gender|income|
    +---+------+------+----+------+------+
    |  3|  null|   5.2|null|  null|  null|
    +---+------+------+----+------+------+
    



```python
#find number of missing values in a particular column:
import pyspark.sql.functions as funct 
num_rows = df_miss.count()
df_miss.agg((num_rows - funct.count("gender")).alias("gender_missing")
           ,(num_rows - funct.count("age")).alias("age_missing")).show()
```

    +--------------+-----------+
    |gender_missing|age_missing|
    +--------------+-----------+
    |             1|          2|
    +--------------+-----------+
    



```python
# Pythonic way of doing it:
# percentage of missing values in each column: 
import pyspark.sql.functions as funct
df_miss.agg(*[
    ((1 - funct.count(c)/funct.count("*"))*100).alias(c+"_miss") for 
    c in df_miss.columns 
]).show()

#df.agg expects comma separated list of arguments. here you are passing list
# *[] before lists unpacks it.
#funct.count('c') counts number of elements in a particular column. number of
# non-missing elements.
#funct.count("*") counts number of rows in all of the data-set.
```

    +-------+-----------------+-----------+-----------------+-----------------+-----------------+
    |id_miss|      weight_miss|height_miss|         age_miss|      gender_miss|      income_miss|
    +-------+-----------------+-----------+-----------------+-----------------+-----------------+
    |    0.0|14.28571428571429|        0.0|28.57142857142857|14.28571428571429|71.42857142857143|
    +-------+-----------------+-----------+-----------------+-----------------+-----------------+
    



```python
# after having determined that a particular column has so many missing values
# e.g. income, and if you have decided to drop that column, then:
df_miss_no_income = df_miss.select([c for c in df_miss.columns if c != "income"])
```


```python
df_miss_no_income.show()
```

    +---+------+------+----+------+
    | id|weight|height| age|gender|
    +---+------+------+----+------+
    |  1| 143.5|   5.6|  28|     M|
    |  2| 167.2|   5.4|  45|     M|
    |  3|  null|   5.2|null|  null|
    |  4| 144.5|   5.9|  33|     M|
    |  5| 133.2|   5.7|  54|     F|
    |  6| 124.1|   5.2|null|     F|
    |  7| 129.2|   5.3|  42|     M|
    +---+------+------+----+------+
    



```python
#if you want to drop observations instead, dropna method can be used.
# dropna method: accepts a threshold parameter related to number of missing
# values in the row. if a row has missing values greater than that threshold,
# it will be dropped.
df_miss_no_income.dropna(thresh=3).show() 
# resultantly, row with id 3 will be dropped.
```

    +---+------+------+----+------+
    | id|weight|height| age|gender|
    +---+------+------+----+------+
    |  1| 143.5|   5.6|  28|     M|
    |  2| 167.2|   5.4|  45|     M|
    |  4| 144.5|   5.9|  33|     M|
    |  5| 133.2|   5.7|  54|     F|
    |  6| 124.1|   5.2|null|     F|
    |  7| 129.2|   5.3|  42|     M|
    +---+------+------+----+------+
    



```python
#imputing missing values in the dataframe, can use fillna method;
# accepts string, integer/float/long.
# to impute mean, median or any other calculated value, you need to first 
# calculate it; 
# can pass it as dict as well of the form: {<col_name>:<value>}
df_miss.agg(funct.mean("weight").alias("weight")).toPandas().to_dict() #returns a dictionary
#of dictionaries.
```




    {'weight': {0: 140.28333333333333}}




```python
list_of_dict_col_avg = df_miss.agg(funct.mean("weight").alias("weight")).toPandas().to_dict('records') 
#using records in to_dict("records") returns list of dictionary
list_of_dict_col_avg[0] # is the required one.
```




    {'weight': 140.28333333333333}




```python

```


```python
# proper way:
all_col_means= df_miss.agg(*[funct.mean(c).alias(c) for c in df_miss_no_income.columns \
                             if c!='gender']).\
toPandas().to_dict("record")[0]
# this dict will have means of all numeric columns. 
# for categorical columns, you can add a specific missing value replacement as:
#because if you dont, then in the categorical columns, null will appear 
# which isn't cool.
```


```python
all_col_means
```




    {'age': 40.399999999999999,
     'height': 5.4714285714285706,
     'id': 4.0,
     'weight': 140.28333333333333}




```python
all_col_means['gender']="missing"
```


```python
all_col_means
```




    {'age': 40.399999999999999,
     'gender': 'missing',
     'height': 5.4714285714285706,
     'id': 4.0,
     'weight': 140.28333333333333}




```python
df_miss_no_income.show()
```

    +---+------+------+----+------+
    | id|weight|height| age|gender|
    +---+------+------+----+------+
    |  1| 143.5|   5.6|  28|     M|
    |  2| 167.2|   5.4|  45|     M|
    |  3|  null|   5.2|null|  null|
    |  4| 144.5|   5.9|  33|     M|
    |  5| 133.2|   5.7|  54|     F|
    |  6| 124.1|   5.2|null|     F|
    |  7| 129.2|   5.3|  42|     M|
    +---+------+------+----+------+
    



```python
#once you have such dictionary, you can use it to impute:
df_miss_no_income.fillna(all_col_means).show()
#if you specify a particular value in fillna, it will fill all of the
#dataframe with that.
#<from_other>can create your own dictionary and pass it as well:
```

    +---+------------------+------+---+-------+
    | id|            weight|height|age| gender|
    +---+------------------+------+---+-------+
    |  1|             143.5|   5.6| 28|      M|
    |  2|             167.2|   5.4| 45|      M|
    |  3|140.28333333333333|   5.2| 40|missing|
    |  4|             144.5|   5.9| 33|      M|
    |  5|             133.2|   5.7| 54|      F|
    |  6|             124.1|   5.2| 40|      F|
    |  7|             129.2|   5.3| 42|      M|
    +---+------------------+------+---+-------+
    



```python
fill_dict={'id':10,'weight':140.28,'height':10,"age":20,'gender':'missing'}
```


```python
df_miss.fillna(fill_dict).show() #even though in above, you didn't specify
#income, but it didn't give error. and just replaced the ones with the values
#and in income column, it kept it null.
```

    +---+------+------+---+-------+------+
    | id|weight|height|age| gender|income|
    +---+------+------+---+-------+------+
    |  1| 143.5|   5.6| 28|      M|100000|
    |  2| 167.2|   5.4| 45|      M|  null|
    |  3|140.28|   5.2| 20|missing|  null|
    |  4| 144.5|   5.9| 33|      M|  null|
    |  5| 133.2|   5.7| 54|      F|  null|
    |  6| 124.1|   5.2| 20|      F|  null|
    |  7| 129.2|   5.3| 42|      M| 76000|
    +---+------+------+---+-------+------+
    



```python
df_miss.na.fill(fill_dict).show() #na.fill and fillna seem equivalent;
```

    +---+------+------+---+-------+------+
    | id|weight|height|age| gender|income|
    +---+------+------+---+-------+------+
    |  1| 143.5|   5.6| 28|      M|100000|
    |  2| 167.2|   5.4| 45|      M|  null|
    |  3|140.28|   5.2| 20|missing|  null|
    |  4| 144.5|   5.9| 33|      M|  null|
    |  5| 133.2|   5.7| 54|      F|  null|
    |  6| 124.1|   5.2| 20|      F|  null|
    |  7| 129.2|   5.3| 42|      M| 76000|
    +---+------+------+---+-------+------+
    



```python

#if you just want to drop all the rows with missing values:
df_miss.na.drop().show()
```

    +---+------+------+---+------+------+
    | id|weight|height|age|gender|income|
    +---+------+------+---+------+------+
    |  1| 143.5|   5.6| 28|     M|100000|
    |  7| 129.2|   5.3| 42|     M| 76000|
    +---+------+------+---+------+------+
    

